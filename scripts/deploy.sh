#!/bin/bash -e

usage() {
PROGRAM=$(basename $0)
echo ""
echo "${PROGRAM} <-k key_file> <-c cert_file> <-s console domain name> <-p privacyfront domain name> <-h db host> <-u db user> <-w db pass> <-r report secret>"
echo ""
exit -1
}

APP_NAME=privacyone-paid-test
APP_NAMESPACE=default
REGISTRY=gcr.io/privacy1-ab-public
TAG=1.8.3

key_file=""
cert_file=""
domain_console=""
domain_pf=""
db_host=""
db_user=""
db_pass=""
rp_secret=""

while getopts "k:c:w:s:p:h:u:w:r:" opt; do
  case ${opt} in
    k )
      key_file=$OPTARG
      echo "key file ${key_file}"
      ;;
    c )
      cert_file=$OPTARG
      echo "certificate file ${cert_file}"
      ;;
    s )
      domain_console=$OPTARG
      echo "Console domain name ${domain_console}"
      ;;
    p )
      domain_pf=$OPTARG
      echo "Privacyfront domain name ${domain_pf}"
      ;;
    h )
      db_host=$OPTARG
      echo "DB host ${db_host}"
      ;;
    u )
      db_user=$OPTARG
      echo "DB user ${db_user}"
      ;;
    w )
      db_pass=$OPTARG
      echo "DB pass ${db_pass}"
      ;;
    r )
      rp_secret=$OPTARG
      echo "report Secret ${db_pass}"
      ;;
    ? )
      usage
      ;;
  esac
done
shift $((OPTIND -1))

echo $license_file
if [ -z "${key_file}" ] || [ -z "${cert_file}" ]; then
  usage
fi

tls_key=$(cat $key_file | base64 -w 0)

tls_crt=$(cat $cert_file | base64 -w 0)

cmd="mpdev /scripts/install --deployer=$REGISTRY/harpocrates/deployer:$TAG \
      --parameters='{"\"name\"": "\"${APP_NAME}\"", \
      "\"namespace\"": "\"${APP_NAMESPACE}\"", \
      "\"db.host\"": "\"${db_host}\"", \
      "\"db.user\"": "\"${db_user}\"", \
      "\"db.password\"": "\"${db_pass}\"", \
      "\"tls.base64EncodedPrivateKey\"": "\"${tls_key}\"", \
      "\"tls.base64EncodedCertificate\"": "\"${tls_crt}\"", \
      "\"domain.console\"": "\"${domain_console}\"", \
      "\"domain.privacyfront\"": "\"${domain_pf}\"", \
      "\"reportingSecret\"": "\"${rp_secret}\""}'"

echo $cmd

eval $cmd
