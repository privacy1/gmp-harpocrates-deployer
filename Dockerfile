FROM marketplace.gcr.io/google/debian10 AS build

RUN apt-get update \
    && apt-get install -y --no-install-recommends gettext

RUN apt-get install -y expat


ADD chart/harpocrates /tmp/chart
RUN cd /tmp && tar -czvf /tmp/harpocrates.tar.gz chart

ADD schema.yaml /tmp/schema.yaml
ADD data-test/schema.yaml /tmp/data-test/schema.yaml

FROM gcr.io/cloud-marketplace-tools/k8s/deployer_helm:latest
COPY --from=build /tmp/harpocrates.tar.gz /data/chart/
COPY --from=build /tmp/data-test/schema.yaml /data-test/
COPY --from=build /tmp/schema.yaml /data/

ENV WAIT_FOR_READY_TIMEOUT 1800
ENV TESTER_TIMEOUT 1800