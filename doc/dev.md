# Build deployer a docker image
	
In the project's root directory
	
```	
$ export REGISTRY=gcr.io/privacy1-ab-public	
$ export APP_NAME=harpocrates
$ export TAG=1.2.0
	

$ docker build --tag $REGISTRY/$APP_NAME/deployer:$TAG .	
$ docker push $REGISTRY/$APP_NAME/deployer:$TAG
	
```

# Run CI
Trigger CI with parameters
```
TAG=1.2
PATCH=0
```
