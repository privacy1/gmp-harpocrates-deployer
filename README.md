# Installation

## Quick install with Google Marketplace
Few clicks to install Harpocrates application to Google Kubernetes cluster using Google Marketplace.
Follow the [on-screen instructions](https://console.cloud.google.com/kubernetes/application/europe-north1-a/p1-harpocrates/harpocrates-ns/harpocrates?filter=solution-type:k8s&q=harpocrates&subtask=details&subtaskValue=privacy1-ab-public%2Fharpocrates&project=privacy1-ab-public&subtaskIndex=3)


## Command line instruction

### Set up command-line tools

You need install below tools on your local workstation.

-   [gcloud](https://cloud.google.com/sdk/gcloud/)
-   [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)
-   [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Following the link [Tool Prerequisites](https://github.com/GoogleCloudPlatform/marketplace-k8s-app-tools/blob/master/docs/tool-prerequisites.md)
to install mpdev

### Configure gcloud as Docker credential helper
```
gcloud auth configure-docker
```
### Create your Mysql database
Create a Mysql database instance in Google Cloud SQL that allowed access from your Kubernetes cluster. Follow the guide [Creating and managing MySQL databases](https://cloud.google.com/sql/docs/mysql/create-manage-databases).

For connectivity configuration, please choose "Private IP". And also choose a "Associated networking" which will be shared with your cluster. You can create a specific network or use "default"

After Mysql instance created, create a username and password.

Create below databases
* cerberusdb
* keychaindb
* keychainanalyticsdb
* cookiejardb
* dpiadb
* ldardb

Choose "Character set" as utf8mb4. Choose "Collation" as 
utf8mb4_unicode_ci.

### Create your cluster in project.
```
$ gcloud container clusters create "your_cluster_name" --zone "your_zone" --enable-ip-alias --subnetwork=subnet-name
```

subnet-name here is same with when you create Mysql instance
### Connect kubectl to your cluster
```
$ gcloud container clusters get-credentials "your_cluster_name" --zone "your_zone"
```

### Clone this repo
```
$ git clone https://gitlab.com/privacy1/gcp-marketplace-harpocrates.git
```

### Config namespace environment variable
```
$ export NAMESPACE=harpocrates-ns
```

### Create namespace
```
$ kubectl create namespace "$NAMESPACE"
```

### Install Application resource definition
```
$ kubectl apply -f "https://raw.githubusercontent.com/GoogleCloudPlatform/marketplace-k8s-app-tools/master/crd/app-crd.yaml"
```

### Deploy APP container
```
$ scripts/deploy.sh <-l license_file> <-k key_file> <-c cert_file> <-s console domain name> <-p privacyfront domain name>
```

# Post installation

## Check app
```
$ kubectl get pod --namespace "${NAMESPACE}"
```

## Update license

When you buy a new license, you can update the license file. Copy the license file to /tmp/license

```
$ kubectl create configmap privacyone-license \
  --namespace ${NAMESPACE} \
  --from-file=license=/tmp/license \
  --dry-run -o yaml | kubectl apply -f -
```
Restart privacyfront service

```
$ kubectl scale statefulset privacyfront --replicas=0 --namespace ${NAMESPACE}
$ kubectl scale statefulset privacyfront --replicas=1 --namespace ${NAMESPACE}
```

## Backup database
Port forward to mysql database service.
```
$ kubectl port-forward --namespace ${NAMESPACE} harpocrates-mysql-0 33060:3306
```
The above command forwards port 33060 to mysql service.

Run the below command to backup Harpocrates databases.
```
$ mysqldump -h 127.0.0.1 -P 33060 -u p1-user -p \
  --databases cerberusdb keychaindb ldardb keychainanalyticsdb dpiadb > ${db_backup_file}
```

## Restore database
```
$ mysql -h 127.0.0.1 --port=33060 -u p1-user -p < ${db_backup_file}
```

## Upgrade application
Each of Harpocrates micro-services can be upgrated independently.

Run commands below and the SERVICE_NAME param can be replaced by one of the followings

* cerberus
* keychain
* keychain-analytics
* ldar
* dpia
* privacy-console
* privacyfront

```
$ export SERVICE_NAME=ldar
$ export SERVICE_IMAGE=gcr.io/privacy1-ab-public/harpocrates/${SERVICE_NAME}:<NEW_VERSION>
$ kubectl patch statefulset ${SERVICE_NAME} --namespace ${NAMESPACE} \
  --type='json' \
  --patch="[{ \
      \"op\": \"replace\", \
      \"path\": \"/spec/template/spec/containers/0/image\", \
      \"value\":\"${SERVICE_IMAGE}\" \
    }]"
```

## Delete application
You can delete application by running
```
$ kubectl delete ${APP_NAME} --namespace ${NAMESPACE}
```
# Release information
## 1.8.2
NA
## 1.8.3
Fix CVE-2021-3711, CVE-2023-34034, CVE-2021-36159
